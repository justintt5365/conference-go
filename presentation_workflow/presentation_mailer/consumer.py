import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    send_mail(
        # subject
        "Your presentation has been accepted",
        # message
        f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted",
        # from_email
        "admin@conference.go",
        # receipt_list
        [content["presenter_email"]],
        fail_silently=False,
    )


def process_reject(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    send_mail(
        # subject
        "Your presentation has been rejected",
        # message
        f"{content['presenter_name']}, your presentation {content['title']} has been rejected",
        # from_email
        "admin@conference.go",
        # receipt_list
        [content["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_reject,
            auto_ack=True,
        )
        print("email sent")
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
