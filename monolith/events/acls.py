import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


# def get_photo(city, state):
#     headers = {"authorization": PEXELS_API_KEY}
#     url = f"https://api.pexels.com/v1/search?query={city}, {state}&per_page=1"
#     response = requests.get(url, headers)
#     content = json.loads(response.content)
#     return {"photo_url": content["photos"][0]["src"]["original"]}


# https://api.pexels.com/v1/search?query=Tampa, FL&per_page=1


def get_photo(city, state):
    headers = {"authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"photo_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"photo_url": None}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "weather": {
                "temp": content["main"]["temp"],
                "description": content["weather"][0]["description"],
            }
        }
    except (KeyError, IndexError):
        return None
